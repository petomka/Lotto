package de.crafttopia.lotto.config;

import de.crafttopia.lotto.Main;

/**
 * Created by Benedikt on 21.07.2017.
 */
public class ConfigManager {

    /**
     * Returns a String from the plugin's config.yml with color-codes replaced and prefix
     * @param path the path to load from
     * @return the formatted String with prefix
     */
    public static String getString(String path) {
        return Main.config.getString("prefix").replaceAll("&", "§").replaceAll("§§", "&")
                + Main.config.getString(path).replaceAll("&", "§").replaceAll("§§", "&").replaceAll("<symbol>", Main.economy.currencyNamePlural());
    }

}
