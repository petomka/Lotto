package de.crafttopia.lotto.commands;

import de.crafttopia.lotto.config.ConfigManager;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;

/**
 * Custom Command-Manager
 */
public class CommandManager {

    private static ArrayList<Command> commands = new ArrayList<>();

    public static void registerCommand(Command cmd) {
        commands.add(cmd);
    }

    public static boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {
        for(Command command : commands) {
            //Names of both commands matches?
            if(cmd.getName().equalsIgnoreCase(command.getName())) {
                //sender has declared permission?
                if(sender.hasPermission(command.getPermission())) {
                    //return if the execution was successful?
                    return command.execute(sender, label, args);
                } else {
                    //well he can't do that unfortunately
                    sender.sendMessage(ConfigManager.getString("nopermission"));
                }
            }
        }
        return true;
    }

}
