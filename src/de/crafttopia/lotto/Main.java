package de.crafttopia.lotto;

import de.crafttopia.lotto.commands.CommandLotto;
import de.crafttopia.lotto.commands.CommandManager;
import de.crafttopia.lotto.game.Game;
import de.crafttopia.lotto.mysql.MySQL;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.logging.Level;

/**
 * Created by Benedikt on 21.07.2017.
 */
public class Main extends JavaPlugin {

    public static Economy economy;
    public static Main main;

    public static FileConfiguration config;

    @Override
    public void onEnable() {
        main = this;
        getLogger().info("Lotto aktiviert!");
        setupEconomy();
        initConfig();
        initMySQL();
        initCommands();
        new Game();
    }

    @Override
    public void onDisable() {
        getLogger().info("Lotto deaktiviert!");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return CommandManager.onCommand(sender, command, label, args);
    }

    private void initCommands() {
        new CommandLotto();
    }

    private void initConfig() {
        /*Initialize config with default values*/
        Main.config = this.getConfig();

        config.addDefault("prefix", "&6&l[&eLotto&6&l]&r ");
        config.addDefault("nopermission", "&cDazu fehlt dir die Berechtigung!");
        config.addDefault("error", "&4Ein Fehler ist aufgetreten.");
        config.addDefault("number-format", "&7Gib eine positive Ganzzahl als Argument an!");
        config.addDefault("double-format", "&7Gib eine Kommazahl als Argument an (zB 1.5)!");

        config.addDefault("message.noplayers", "&cNiemand hat für diese Ziehung ein Ticket gekauft!");
        config.addDefault("message.remind-next-start", "&eDie nächste Ziehung ist am &b<date> &eum &b<time> Uhr&e!");
        config.addDefault("message.remind-start", "&7Noch &b<time> &7Minute(n) bis zur nächsten Ziehung! Derzeitiger Jackpot: &c<jackpot> &e<symbol>");
        config.addDefault("message.ticket.singular", "Ticket");
        config.addDefault("message.ticket.plural", "Tickets");
        config.addDefault("message.broadcast-buy", "&c<player> &7hat <amount> <g_ticket> gekauft! &eMomentaner Jackpot: &c<jackpot> &e<symbol>");
        config.addDefault("message.notify-bought", "&7Du hast <amount> <g_ticket> für &c<cost> &e<symbol> &7gekauft.");
        config.addDefault("message.insufficient-funds", "&7Du hast leider &cnicht genug Geld&7!");
        config.addDefault("message.ticketlimit", "&aDu kannst maximal &e<amount> &aTickets kaufen.");
        config.addDefault("message.ticket-buy-left", "&aDu kannst nur noch &e<amount> &aTickets kaufen.");
        config.addDefault("message.maxtickets", "&aDu hast bereits das Limit von &e<amount> &aTickets erreicht.");
        config.addDefault("message.winner", "&c&l<player> &ehat soeben den Jackpot von &c<jackpot> &c<symbol> &egewonnen!");
        config.addDefault("message.won", "&7Deinem Account wurden &e<jackpot> <symbol> &7hinzugefügt.");
        config.addDefault("message.status", "&7Infos zu diesem Spiel:\n" +
                "&7Momentaner Jackpot: &c<jackpot> &e<symbol>\n" +
                "&7Teilnehmende Spieler: &c<players>\n" +
                "&7Insgesamt spielende Tickets: &c<tickets_total>\n" +
                "&7Ende dieser Ziehung: &b<time> Uhr\n" +
                "&7Preis für ein Ticket: &c<ticket_price> &e<symbol>\n" +
                "&7Anzahl gekaufter Tickets: &c<tickets_bought>\n" +
                "&7Deine aktuelle Gewinnchance: &c<win_percentage>%\n" +
                "&7Letzter Gewinner: &6<last_winner> &7gewann &c<last_winner_amount> <symbol>");
        config.addDefault("message.addedpot", "&7Dem Jackpot wurden &c<amount> &e<symbol> &7hinzugefügt. Neuer Jackpot: &c<jackpot> &e<symbol>");
        config.addDefault("message.removedpot", "&7Dem Jackpot wurde &c<amount> &e<symbol> &7abgezogen. Neuer Jackpot: &c<jackpot> &e<symbol>");
        config.addDefault("message.broadcast-change", "&7Neuer Jackpot: &c<jackpot> &e<symbol>");

        config.addDefault("pot-start", 2000.0d);
        config.addDefault("ticket-price", 100.0d);
        config.addDefault("max-tickets", 5);

        config.addDefault("draw-times", Arrays.asList("11:00", "15:00", "19:00"));

        config.addDefault("reminder-times", Arrays.asList(60, 40, 20, 10, 5, 2, 1));

        config.options().copyDefaults(true);
        this.saveConfig();
    }

    private void initMySQL() {
        /*Init MySQL Database class*/
        try {
            MySQL.initMySQL();
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Could not connect to database. Disabling!");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        try {
            /*and create the needed tables*/
            MySQL.executeUpdate("create table if not exists lotto_current_game_tickets (`id` int not null auto_increment, " +
                    "`uuid` varchar(50) character set utf8 collate utf8_bin not null, primary key(`id`)) engine = MyISAM;");
            MySQL.executeUpdate("create table if not exists lotto_current_game_props (`id` int not null, `added_pot` double not null, " +
                    "`draw_time` bigint not null, primary key(`id`)) engine = MyISAM");
            MySQL.executeUpdate("create table if not exists lotto_winners (`id` int not null auto_increment, `uuid` varchar(50) character set utf8 collate utf8_bin not null," +
                    "`amount_won` double not null, primary key (`id`)) engine = MyISAM");
            /*This database has no use and can hence be deleted*/
            //MySQL.executeUpdate("create table if not exists lotto_offline_winners (`uuid` varchar(50) character set utf8 collate utf8_bin not null , " +
            //        "`amount` double not null , primary key(`uuid`)) engine = MyISAM");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean setupEconomy() {
        /*Code taken from Vault API GitHub page*/
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        economy = rsp.getProvider();
        return economy != null;
    }

}
